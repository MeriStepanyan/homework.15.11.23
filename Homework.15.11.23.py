import math
# 1.Write a program that prints the ASCII value of each character in a given string using a 'for' loop.
string="AdKl"
ascii_str=string.encode("ascii")
for i,j in zip(ascii_str,string):
    print(j,"->",i)
# 2. Iterate through a dictionary and print both keys and values.
di={
    "name":"Ann",
    "age": 13
} 
print("Dictionary items: ")
for i in di.items():
    print(i)  

# 3. Calculate the sum of all prime numbers between 1 and 20 using  'for' loops.

def is_prime(number):
    count=0
    for i in range (2,int(math.sqrt(number)+1)): 
        if(number%i==0):
            count+=1
        else:
            continue    
    if(count==0):
        return True
    else:
        return False
##print(is_prime(19))    
def calc_sum_primes(start, end):
    start+=1
    sum=0
    while(start!=end):
        if(is_prime(start)):
            sum+=start
        start+=1
    return sum  
print("The sum of all prime numbers between 1 and 20: ",calc_sum_primes(1,20))  

# 4. Implement a program that calculates the sum of all multiples of 3 between 1 and 50.
def sum_mul_3(start, end):
    sum1=0
    for i in range(start,end):
        if(i%3==0):
            sum1+=i
    return sum1
print("The sum of all multiples of 3 between 1 and 50: ",sum_mul_3(1,50))   

# 5. Write a list comprehension to generate a list of even numbers from 1 to 10.
even_1_10=[x for x in range(1,10) if x%2==0]
print(even_1_10)

# 6. Write a dictionary comprehension to create a dictionary of squares for numbers from 1 to 5.
Dict = {x: x**2 for x in range(1,6)}
print ("The dictionary of squares for numbers from 1 to 5",Dict)

# 7. Create a program utilizing list comprehension to generate a matrix ranging from 1 to 10.
matrix=[[column+row for column in range(1,4)] for row in range(0,9,3)]
print("Matrix 1-9",matrix)

# 8. Create a function that takes a matrix as input and returns its transpose (rows become columns and vice versa).
def transpose(mtrx):
    new_mtrx=[]
    for i in range(3):
        m=[]
        for j in range(3):
           m.append(mtrx[j][i])
        new_mtrx.append(m)
    return new_mtrx   
mtrx=[[1, 2, 3], [4, 5, 6], [7, 8, 9]]
print("The new transposed matrix: ",transpose(mtrx))

# 9. Implement a function to rotate a square matrix 90 degrees clockwise.
def transpose(mtrx):
    n=len(mtrx[0])
    for i in range(n//2):
        for j in range(i,n-i-1):
            tmp=mtrx[i][j]
            mtrx[i][j]=mtrx[n-j-1][i]
            mtrx[n-j-1][i]=mtrx[n-i-1][n-j-1]
            mtrx[n-i-1][n-j-1]=mtrx[j][n-i-1]
            mtrx[j][n-i-1]=tmp
    return mtrx  
mtrx=[[1, 2, 3], [4, 5, 6], [7, 8, 9]]
print(transpose(mtrx))